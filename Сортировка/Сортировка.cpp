﻿#include <iostream>
using namespace std;

class Players
{
public:
	char name[256] = "Imya";
	int score = 0;

	void EnterNameScore()
	{
		cin >> name >> score;
	}
	void ShowNameScore()
	{
		cout << name << "   " << score << endl;
	}
};

int main()
{
	int size = 0;
	cout << "VVedite kolichestvo igrakov: ";
	cin >> size;
	Players* Massiv = new Players[size];

	for (int i = 0; i < size; ++i)
	{
		cout << "Vvedite " << i + 1 << " imya igroka i ego o4ki:";
		Massiv[i].EnterNameScore();
	}

	cout << endl;
	cout << "Ne sortirovano:" << endl;

	for (int i = 0; i < size; ++i)
	{
		Massiv[i].ShowNameScore();
	}

	for (int i = 0; i < size; i++)
	{
		for (int j = size - 1; j > i; j--)
		{
			if ((Massiv[j].score) > (Massiv[j - 1].score))
			{
				swap(Massiv[j], Massiv[j - 1]);
			}
		}
	}

	cout << endl;
	cout << "Sortirovanno:" << endl;

	for (int i = 0; i < size; ++i)
	{
		Massiv[i].ShowNameScore();
	}
}
